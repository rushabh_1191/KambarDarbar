<?php
/**
 * Created by PhpStorm.
 * User: rushabh
 * Date: 16/07/15
 * Time: 11:19 PM
 */


include "db_connect.php";
$RECEIPTS = 1;
$CASE_PAPER = 0;
$treatment_filter = 1;
$department_filter = 2;
$doctor_type = 3;
$addedConditionData = "";
$addedConditionAmount = "";
$viewType = $CASE_PAPER;
$addedDateConditionAmount = "";
$queryData = json_decode(file_get_contents("php://input"), true);


$treatmentId = -1;
$doctorId = -1;
$departmentId = -1;
$treatmentType = -1;
$hasFilters = false;
if (isset($queryData["view_type"])) {
    $viewType = $queryData["view_type"];
}
if (isset($queryData["doctor_id"])) {
    $hasFilters = true;
    $doctorId = $queryData["doctor_id"];
}
if (isset($queryData["treatment_id"])) {
    $hasFilters = true;
    $treatmentId = $queryData["treatment_id"];
}

if (isset($queryData["department_id"])) {
    $hasFilters = true;
    $departmentId = $queryData["department_id"];
}
if (isset($queryData["treatment_type"])) {
    $hasFilters = true;
    $treatmentType = $queryData["treatment_type"];
}

$page_number = $queryData["page_number"];
$dateCondition = "";

if ($hasFilters) {
    $addedConditionData = " WHERE ";
    $addedConditionAmount = " WHERE ";
}
if (isset($queryData["doctor_id"])) {
    $addedConditionData .= " c.case_paper_doctor_id = " . $doctorId;
    $addedConditionAmount .= " case_paper_doctor_id = " . $doctorId;

}

if ($treatmentId > 0) {
    if ($doctorId > 0) {
        $addedConditionData .= " AND ";
        $addedConditionAmount .= " AND ";
    }
    $addedConditionData .= " t.treatment_id=" . $treatmentId;
    $addedConditionAmount = " WHERE case_paper_treatment_id=" . $treatmentId;
}

if ($departmentId > 0) {
    if ($treatmentId > 0 | $doctorId > 0) {
        $addedConditionData .= " AND ";
        $addedConditionAmount .= " AND ";
    }
    $addedConditionData .= "d.department_id=" . $departmentId;
    $addedConditionAmount .= "treatment_information.treatment_department_id=" . $departmentId;
}

if ($treatmentType >= 0) {
    if ($treatmentId > 0 | $doctorId > 0 | $departmentId > 0) {
        $addedConditionData .= " AND ";
        $addedConditionAmount .= " AND ";
    }
    $addedConditionData .= "t.is_rate_subsidized=" . $treatmentType;
    $addedConditionAmount .= "treatment_information.is_rate_subsidized=" . $treatmentType;
}


if (isset($queryData["from"])) {


    if (!$hasFilters) {
        $dateCondition = " WHERE ";

    } else {
        $dateCondition = " AND (";

    }

    $dateCondition = $dateCondition . "case_paper_date >='" . $queryData["from"] . "' AND case_paper_date <= ' " . $queryData["till"] . "'";

    if ($hasFilters) {
        $dateCondition = $dateCondition . ")";
    }
    $addedDateConditionAmount = $dateCondition;

}


$total_limit = 10;
$query="";


if ($viewType == $CASE_PAPER) {
    $queryOrderBy=" ORDER BY c.case_paper_date DESC LIMIT ";
    $queryEnd=$addedConditionData . $dateCondition .
        $queryOrderBy . $total_limit . " OFFSET " . $total_limit * $page_number;

    $query = "SELECT * FROM case_paper_information c
INNER JOIN receipt_information r
on c.case_paper_id=r.case_paper_id
INNER JOIN patient_information p
ON c.casepaper_patient_id=p.patient_id
INNER JOIN treatment_information t
ON c.case_paper_treatment_id=t.treatment_id
INNER JOIN department_information d
on t.treatment_department_id=d.department_id
INNER JOIN doctor_information dr
ON dr.doctor_id=c.case_paper_doctor_id
" .$queryEnd;
} else {

    $queryOrderBy=" ORDER BY r.receipt_date DESC LIMIT ";
    $queryEnd=$addedConditionData . $dateCondition .
        $queryOrderBy . $total_limit . " OFFSET " . $total_limit * $page_number;
    $query = "SELECT * FROM receipt_information r
INNER JOIN case_paper_information c
ON r.case_paper_id=c.case_paper_id
INNER JOIN treatment_information t
ON t.treatment_id=c.case_paper_treatment_id
INNER JOIN patient_information p
ON p.patient_id=c.casepaper_patient_id
INNER JOIN doctor_information dr
ON dr.doctor_id=c.case_paper_doctor_id
INNER JOIN department_information d
ON  t.treatment_department_id=d.department_id ". $queryEnd;
}

//echo $query;
$result = mysqli_query($conn, $query);

$full_data = array();
$casepaperdata = array();
while ($row = $result->fetch_assoc()) {

    $data = array();
    foreach ($row as $key => $value) {
        $data[$key] = $value;
    }

    array_push($casepaperdata, $data);
}

$full_data["case_paper_list"] = $casepaperdata;

$query = "SELECT SUM(case_paper_information.case_paper_fees) AS business,
SUM(case_paper_information.case_paper_fees_paid) AS collection,SUM(treatment_actual_fees) AS actual_treatment_fees,
SUM(treatment_fees) AS subisidised_treatment_fees
FROM case_paper_information
INNER JOIN treatment_information ON case_paper_information.case_paper_treatment_id=treatment_information.treatment_id"
    . $addedConditionAmount . $addedDateConditionAmount;

$result = mysqli_query($conn, $query);
$collectionInfo = array();
while ($row = $result->fetch_assoc()) {
    foreach ($row as $key => $value) {
        $collectionInfo[$key] = $value;
    }
}
$full_data["collection_info"] = $collectionInfo;


echo json_encode($full_data);


mysqli_close($conn);
?>