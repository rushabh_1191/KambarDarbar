<!DOCTYPE html>
<html ng-app="login_user">

<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kambar darbar</title>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ui-bootstrap-tpls-0.13.0.min.js"></script>

    <link href="css/bootstrap.min.css" rel="stylesheet"/>

    <link href="css/style.css" rel="stylesheet"/>

    <script>


        var app = angular.module('login_user', ['ui.bootstrap']);

        app.controller("login_user_controller", function ($scope, $http) {

            $scope.checkLogin=function(){

                $scope.formData={"user_name":$scope.user_name,"user_password":$scope.user_password};

                $scope.form={"user_information":$scope.formData}

                $http.post("check_login.php",$scope.form).success(function(login_response){

                    console.log(login_response);
                    if(login_response.status=="success") {
                        $scope.loginFailed=false;
                        window.top.location = "index.php";
                    }
                    else

                    {
                        $scope.loginFailed=true;
                        $scope.login_error=login_response.message;
                    }
                });
            }
        });
    </script>
</head>

<body>
<div class="row" ng-controller="login_user_controller">

    <div class="col-lg-4 col-lg-offset-4">
        <div class="panel panel-primary margin-class">
            <div class="panel-heading">
                Kambar Darbar | Login
            </div>

            <div class="panel-body margin-class">
                <input type="text" class="form-control margin-class" ng-model="user_name" placeholder="Username"/>
                <input type="password" class="form-control margin-class"placeholder="Password" ng-model="user_password" />

                <div class="small-padding-class alert-danger" ng-bind="login_error" ng-show="loginFailed"></div>

                <input type="button" class="btn form-control btn-primary margin-class"  ng-click="checkLogin()" value="Login">



            </div>
        </div>
    </div>
</div>

</body>
</html>