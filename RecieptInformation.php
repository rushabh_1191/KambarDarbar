<html>
<head>
    <title>Kambar darbar</title>
    <script src="js/angular.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery-ui.js"></script>
    <script>

        var app = angular.module("ShowPatients", []);

        app.controller("ShowPatientsController", function ($scope, $http, $filter) {

            var TREATMENT_FILTER = 1;
            var DEPARTMENT_FILTER = 2;
            var DOCTOR_FILTER = 3;

            $scope.page_number = 0;

            $scope.selectedFilterId = -1;
            $scope.selectedFilterType = -1;
            $scope.selectedFrom = undefined;
            $scope.selectedTill = undefined;

            $scope.selectedDepartmentId = undefined;
            $scope.selectedTreatmentId = undefined;
            $scope.selectedDoctorId = undefined;
            $scope.isFurtherFetchNeeded = true;

            $scope.isFilterActive = false
            $scope.GetDepartment = function () {

                $http.get("GetDepartment.php").success(function (content) {


                    $scope.departmentList = content.data;
                });
            };

            $scope.GetTreatment = function () {

                $http.get("GetTreatment.php").success(function (content) {
                    $scope.treatmentList = content;
                });
            }

            $scope.GetDoctors = function () {
                $http.get("GetDoctors.php").success(function (content) {
                    $scope.doctorList = content.data;
                });
            }

            $scope.filterByTreatment = function () {
//                $scope.GetPatientInformation();
                $treatmentInfo = JSON.parse($scope.selectedTreatment);
                $scope.selectedTreatmentId = $treatmentInfo.treatment_id

                $scope.isFilterActive = true;
                $scope.resetDepartmentSelection();
                $scope.resetDoctorSelection()
                $scope.selectedFilterId = $scope.selectedTreatmentId;
                $scope.selectedFilterType = TREATMENT_FILTER
                $scope.page_number = 0;
                $scope.GetPatientInformation()

            }


            Date.prototype.yyyymmdd = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]) + " 00:00:00"; // padding
            };

            $scope.removeAllFilters = function () {

                $scope.selectedFilterId = -1;
                $scope.selectedFilterType = -1;
                $scope.resetDepartmentSelection();
                $scope.resetTreatmentSelection();
                $scope.resetDoctorSelection();
                $scope.isFilterActive = false;
                $scope.page_number = 0;
                $scope.GetPatientInformation();
            }

            $scope.resetDepartmentSelection = function () {
                $scope.selectedDepartmentId = undefined;
                $scope.selectedDepartment = undefined;

            }

            $scope.resetDoctorSelection = function () {
                $scope.selectedDoctorId = undefined;
                $scope.selectedDoctor = undefined;
            }

            $scope.resetTreatmentSelection = function () {
                $scope.selectedTreatment = undefined;
                $scope.selectedDepartmentId = undefined;

            }


            $scope.filterByDoctor = function () {

                $doctorInfo = JSON.parse($scope.selectedDoctor);
                $scope.selectedDoctorId = $doctorInfo.doctor_id

                $scope.selectedFilterId = $scope.selectedDoctorId;
                $scope.selectedFilterType = DOCTOR_FILTER;

                $scope.isFilterActive = true;

                $scope.resetDepartmentSelection();
                $scope.resetTreatmentSelection()
                $scope.page_number = 0;
                $scope.GetPatientInformation();
            }
            $scope.filterByDepartment = function () {

                $departmentInfo = JSON.parse($scope.selectedDepartment)
                $scope.selectedDepartmentId = $departmentInfo.department_id

                $scope.selectedFilterId = $scope.selectedDepartmentId;
                $scope.selectedFilterType = DEPARTMENT_FILTER;
                $scope.isFilterActive = true;

                $scope.resetTreatmentSelection();
                $scope.resetDoctorSelection()
                $scope.page_number = 0;
                $scope.GetPatientInformation($scope.selectedDepartmentId, DEPARTMENT_FILTER)
            };
            $scope.GetPatientInformation = function () {


                $scope.formData = {"filter_type": $scope.selectedFilterType, "filter_id": $scope.selectedFilterId};//,"from":$scope.fromDate.value,"till":$scope.till.value}

                if ($scope.selectedFrom != undefined && $scope.selectedTill != undefined) {
                    $scope.formData.from = $scope.selectedFrom;
                    $scope.formData.till = $scope.selectedTill
                    console.log($scope.formData);
                }
                $scope.formData.page_number = $scope.page_number;


                $http.post("GetAllReceipts.php", $scope.formData).success(function (data) {

                    $scope.a = JSON.stringify(data);

                    $scope.fullData = JSON.parse($scope.a);
                    $scope.casepaperList = $scope.fullData.case_paper_list

                    $scope.collectionInf = $scope.fullData.collection_info

                    $scope.business = $scope.collectionInf.business;
                    $scope.collection = $scope.collectionInf.collection;
                });
            }

            $scope.filterFromDate = function () {
                var dString = $("#fromDate").val();
                var dd = new Date(dString);
                $scope.selectedFrom = dd.yyyymmdd();

            }

            $scope.filterTillDate = function () {
                var dString = $("#tillDate").val();
                var dd = new Date(dString);
                $scope.selectedTill = dd.yyyymmdd();
            }
            $scope.applyDateFilter = function () {


                if ($scope.selectedFrom != undefined && $scope.selectedTill != undefined) {
                    $scope.page_number = 0;
                    $scope.GetPatientInformation();

                }


            }
            $scope.GetInformation = function () {
                $scope.manageNextPrevious();
                $scope.GetPatientInformation(-1, -1);
                $scope.GetTreatment();
                $scope.GetDepartment();
                $scope.GetDoctors();

            }

            $scope.removeDateFilter = function () {
                $scope.selectedFrom = undefined;
                $scope.selectedTill = undefined;
                $scope.fromDate = undefined;
                $scope.till = undefined;
                $scope.page_number = 0;
                $scope.GetPatientInformation();
            }
            $scope.convertDate = function ($dateString) {

                return new Date($dateString);
            };


            $scope.fetchNext = function () {
                if ($scope.isFurtherFetchNeeded) {
                    $scope.page_number++;
                    $scope.GetPatientInformation();
                }
            }
            $scope.fetchPrevious = function () {
                if ($scope.page_number > 0) {
                    $scope.page_number--;
                    $scope.isFurtherFetchNeeded = true;
                    $scope.GetPatientInformation();
                }
            }

            $scope.manageNextPrevious = function () {
                if ($scope.page_number == 0) {
                    $scope.hasPrevious = false;
                }
            }
        });
    </script>
</head>

<body>

<div ng-app="ShowPatients" ng-controller="ShowPatientsController" data-ng-init="GetInformation()">

    <div class="row margin-class">

        <div class="col-lg-2 col-lg-offset-2 alert-info">
            <h3> Total business
                <div>{{business}}</div>
            </h3>
        </div>

        <div class="col-lg-2 alert-success">
            <h3> Received
                <div>{{collection}}</div>
            </h3>
        </div>

        <div class="col-lg-2 alert-danger">

            <h3>
                Balance
                <div>{{business-collection}}</div>
            </h3>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-10 col-lg-offset-1">

            <div class="row">
                <div class="col-lg-4">
                    From : <input id="fromDate" type="date" class="form-control"
                                  data-date-format="dd/mm/yyyy" ng-model="fromDate.value"
                                  ng-change="filterFromDate()"> </span>
                </div>


                <div class="col-lg-4">
                    To : <input type="date" id="tillDate" type="date" class="form-control" ng-model="till.value"
                                ng-change="filterTillDate()"
                                placeholder="Till"/>
                </div>

                <div class="col-lg-1 margin-class">
                    <button class="btn btn-primary" ng-click="applyDateFilter()">Apply</button>
                </div>

                <div class="col-lg-1 margin-class">

                    <button class="btn btn-danger" ng-click="removeDateFilter()">Remove date filter</button>
                </div>


            </div>

            <div class="row margin-class">
                <div class="col-lg-3">
                    Filter by department
                    <select class="form-control col-lg-4" ng-model="selectedDepartment"
                            ng-change="filterByDepartment()">

                        <option value={{department}} ng-repeat="department in departmentList">
                            {{department.department_name}}
                    </select>
                </div>

                <div class="col-lg-3">

                    Filter by treatment
                    <select class="form-control col-lg-4" ng-model="selectedTreatment" ng-change="filterByTreatment()">

                        <option value={{treatment}} ng-repeat="treatment in treatmentList">
                            {{treatment.treatment_title}}
                        </option>

                    </select>
                </div>

                <div class="col-lg-3">

                    Filter by doctor
                    <select class="form-control col-lg-4" ng-model="selectedDoctor" ng-change="filterByDoctor()">

                        <option value={{doctor}} ng-repeat="doctor in doctorList">
                            {{doctor.doctor_name}}
                        </option>

                    </select>

                </div>

                <div class="col-lg-3">
                    <input type="button" class="btn btn-primary btn-danger margin-class" ng-show="isFilterActive"
                           value="Remove filters" ng-click="removeAllFilters()"/>
                </div>


            </div>


            <nav>
                <ul class="pager">
                    <li class="previous" ng-enabled="hasPrevious" ng-click="fetchPrevious()"><a href="#"><span
                                aria-hidden="true">&larr;</span> Older</a></li>
                    <li class="next" ng-enabled="hasNext" ng-click="fetchNext()"><a href="#">Newer <span
                                aria-hidden="true">&rarr;</span></a></li>
                </ul>
            </nav>

            <table id="casepaperlist" class="table table-bordered table-hover table-condensed" border="1">

                <tr>
                    <th>Receipt No.</th>
                    </tg>
                    <th>Case Id</th>
                    <th>Patient name</th>
                    <th>Treatment undertaken</th>
                    <th>Treatment fees</th>
                    <th>Fees paid</th>
                    <th>Department name</th>
                    <th> Receipt Date</th>
                    <th>Case paper date</th>
                    <th>Doctor name</th>
                    <th>Reference number</th>
                </tr>

                <tr ng-repeat="casepaper in casepaperList">
                    <td>{{casepaper.receipt_id}}</td>
                    <td>{{casepaper.case_paper_id}}</td>
                    <td>{{casepaper.patient_name}}</td>

                    <td>{{casepaper.treatment_title}}</td>
                    <td>{{casepaper.case_paper_fees}}</td>
                    <td>{{casepaper.amount_paid}}</td>
                    <td>{{casepaper.department_name}}</td>
                    <td>{{convertDate(casepaper.receipt_date) | date: 'dd/MM/yyyy'}}</td>
                    <td>{{convertDate(casepaper.date_of_admission) | date: 'dd/MM/yyyy'}}</td>
                    <td>{{casepaper.doctor_name}}</td>
                    <td>{{casepaper.free_patient_reference_id}}</td>
                </tr>
            </table>
        </div>


    </div>

</div>


</body>
</html>