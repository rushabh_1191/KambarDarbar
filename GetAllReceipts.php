<?php
include "db_connect.php";

$treatment_filter = 1;
$department_filter = 2;
$doctor_type = 3;
$addedConditionData = "";
$addedConditionAmount = "";

$addedDateConditionAmount="";
$queryData = json_decode(file_get_contents("php://input"), true);

$filterId = $queryData["filter_id"];
$filterType = $queryData["filter_type"];

$page_number=$queryData["page_number"];
$dateCondition = "";

if (isset($queryData["from"])) {


    if ($filterType == -1) {
        $dateCondition = " WHERE ";

    } else {
        $dateCondition = " AND (";

    }

    $dateCondition = $dateCondition . "case_paper_date >='" . $queryData["from"] . "' AND case_paper_date <= ' " . $queryData["till"] . "'";

    if ($filterType != -1) {
        $dateCondition = $dateCondition . ")";
    }
    $addedDateConditionAmount = $dateCondition;

}

if ($filterType != -1) {

    if ($filterType == $treatment_filter) {
        $addedConditionData = " WHERE t.treatment_id=" . $filterId;
        $addedConditionAmount = " WHERE case_paper_treatment_id=" . $filterId;

    } else if ($filterType == $department_filter) {
        $addedConditionData = " WHERE d.department_id=" . $filterId;
        $addedConditionAmount = " INNER JOIN treatment_information ON case_paper_information.case_paper_treatment_id=treatment_information.treatment_id WHERE treatment_information.treatment_department_id=" . $filterId;
    } else {
        $addedConditionData = " WHERE c.case_paper_doctor_id = " . $filterId;
        $addedConditionAmount = " WHERE case_paper_doctor_id = " . $filterId;
    }
}

$total_limit=300;


$query="SELECT * FROM receipt_information
INNER JOIN case_paper_information
ON receipt_information.case_paper_id=case_paper_information.case_paper_id
INNER JOIN treatment_information
ON treatment_information.treatment_id=case_paper_information.case_paper_treatment_id
INNER JOIN patient_information
ON patient_information.patient_id=case_paper_information.casepaper_patient_id
INNER JOIN doctor_information
ON doctor_information.doctor_id=case_paper_information.case_paper_doctor_id
INNER JOIN department_information
ON  treatment_information.treatment_department_id=department_information.department_id
 ORDER BY receipt_id DESC";

$result = mysqli_query($conn, $query);

$full_data = array();
$casepaperdata = array();
while ($row = $result->fetch_assoc()) {

    $data = array();
    foreach ($row as $key => $value) {
        $data[$key] = $value;
    }

    array_push($casepaperdata, $data);
}

$full_data["case_paper_list"] = $casepaperdata;


$query = "SELECT SUM(case_paper_information.case_paper_fees) as business,SUM(case_paper_information.case_paper_fees_paid) as collection FROM case_paper_information " . $addedConditionAmount  . $addedDateConditionAmount;

$result = mysqli_query($conn, $query);
$collectionInfo = array();
while ($row = $result->fetch_assoc()) {
    foreach ($row as $key => $value) {
        $collectionInfo[$key] = $value;
    }
}
$full_data["collection_info"] = $collectionInfo;



echo json_encode($full_data);
mysqli_close($conn);
?>