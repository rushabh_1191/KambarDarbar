<?php
include "login_verify.php";
?>


<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Kambar darbar</title>
    <script src="js/angular.min.js"></script>
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <link href="css/style.css" rel="stylesheet"/>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/jquery-ui.js"></script>
    <script>

        var app = angular.module("ShowPatients", []);

        app.controller("ShowPatientsController", function ($scope, $http, $filter) {


            $scope.page_number = 0;
            $scope.SHOW_CASE_PAPER = 0;
            $scope.SHOW_RECEIPTS = 1;

            $scope.selectedFrom = undefined;
            $scope.selectedTill = undefined;

            $scope.selectedDepartmentId = undefined;
            $scope.selectedTreatmentId = undefined;
            $scope.selectedDoctorId = undefined;
            $scope.selectedTreatmentType = undefined;

            $scope.viewType = $scope.SHOW_CASE_PAPER;


            $scope.isFurtherFetchNeeded = true;

            $scope.isFilterActive = false
            $scope.GetDepartment = function () {

                $http.get("GetDepartment.php").success(function (content) {


                    $scope.departmentList = content.data;
                });
            };


            $scope.GetTreatment = function () {

                $http.get("GetTreatment.php").success(function (content) {
                    $scope.treatmentList = content;
                });
            }

            $scope.GetDoctors = function () {
                $http.get("GetDoctors.php").success(function (content) {
                    $scope.doctorList = content.data;
                });
            }

            $scope.filterByTreatment = function () {
//                $scope.GetPatientInformation();
                $treatmentInfo = JSON.parse($scope.selectedTreatment);
                $scope.selectedTreatmentId = $treatmentInfo.treatment_id
                $scope.isFilterActive = true;
                $scope.page_number = 0;
                $scope.GetPatientInformation()

            }

            $scope.filterByTreatmentType = function () {
                $scope.page_number = 0;
                $scope.isFilterActive = true;
                $scope.GetPatientInformation();

            }


            Date.prototype.yyyymmdd = function () {
                var yyyy = this.getFullYear().toString();
                var mm = (this.getMonth() + 1).toString(); // getMonth() is zero-based
                var dd = this.getDate().toString();
                return yyyy + "-" + (mm[1] ? mm : "0" + mm[0]) + "-" + (dd[1] ? dd : "0" + dd[0]) + " 00:00:00"; // padding
            };

            $scope.removeAllFilters = function () {
                $scope.resetDepartmentSelection();
                $scope.resetTreatmentSelection();
                $scope.resetDoctorSelection();
                $scope.resetTreatmentType();
                $scope.page_number = 0;
                $scope.GetPatientInformation();
            }

            $scope.resetDepartmentSelection = function () {
                $scope.selectedDepartmentId = undefined;
                $scope.selectedDepartment = undefined;
            }
            $scope.removeDepartmentFilter = function () {
                $scope.resetDepartmentSelection();
                $scope.GetPatientInformation();
            }

            $scope.resetDoctorSelection = function () {
                $scope.selectedDoctorId = undefined;
                $scope.selectedDoctor = undefined;
            }

            $scope.removeDoctorFilter = function () {
                $scope.resetDoctorSelection();
                $scope.GetPatientInformation();
            }

            $scope.resetTreatmentSelection = function () {
                $scope.selectedTreatment = undefined;
                $scope.selectedTreatmentId = undefined;

            }
            $scope.removeTreatmentFilter = function () {
                $scope.resetTreatmentSelection();
                $scope.GetPatientInformation();
            }

            $scope.filterByDoctor = function () {

                $doctorInfo = JSON.parse($scope.selectedDoctor);
                $scope.selectedDoctorId = $doctorInfo.doctor_id


                $scope.isFilterActive = true;

                $scope.page_number = 0;
                $scope.GetPatientInformation();
            }
            $scope.filterByDepartment = function () {

                $departmentInfo = JSON.parse($scope.selectedDepartment)
                $scope.selectedDepartmentId = $departmentInfo.department_id
                $scope.isFilterActive = true;
                $scope.page_number = 0;
                $scope.GetPatientInformation()
            };

            $scope.resetTreatmentType = function () {
                $scope.selectedTreatmentType = undefined;
                $
            }

            $scope.removeTypeFilter = function () {
                $scope.resetTreatmentType();
                $scope.GetPatientInformation();
            }
            $scope.GetPatientInformation = function () {

                $scope.formData = {
                    "department_id": $scope.selectedDepartmentId,
                    "treatment_id": $scope.selectedTreatmentId,
                    "doctor_id": $scope.selectedDoctorId,
                    "treatment_type": $scope.selectedTreatmentType,
                    "view_type": $scope.viewType
                };


                if ($scope.selectedFrom != undefined && $scope.selectedTill != undefined) {
                    $scope.formData.from = $scope.selectedFrom;
                    $scope.formData.till = $scope.selectedTill
                    console.log($scope.formData);
                }
                $scope.formData.page_number = $scope.page_number;


                $http.post("GetAllPatients.php", $scope.formData).success(function (data) {

                    $scope.a = JSON.stringify(data);

                    $scope.fullData = JSON.parse($scope.a);
                    $scope.casepaperList = $scope.fullData.case_paper_list

                    if ($scope.casepaperList != undefined) {
                        if ($scope.casepaperList.length < 10) {
                            $scope.isFurtherFetchNeeded = false;

                        }
                    }
                    else {
                        $scope.isFurtherFetchNeeded = false;
                    }
                    $scope.collectionInf = $scope.fullData.collection_info

                    $scope.business = $scope.collectionInf.business;
                    $scope.collection = $scope.collectionInf.collection
                    console.log(typeof ($scope.collectionInf.subisidised_treatment_fees));
                    console.log($scope.collectionInf.actual_treatment_fees);
                    $scope.actual_fees = $scope.collectionInf.actual_treatment_fees;
                    $scope.subsidised_fees = $scope.collectionInf.subisidised_treatment_fees


                });
            }

            $scope.changeViewType = function () {
                $scope.GetPatientInformation();
            }

            $scope.filterFromDate = function () {
                var dString = $("#fromDate").val();
                var dd = new Date(dString);
                $scope.selectedFrom = dd.yyyymmdd();

            }

            $scope.filterTillDate = function () {
                var dString = $("#tillDate").val();
                var dd = new Date(dString);
                $scope.selectedTill = dd.yyyymmdd();
            }
            $scope.applyDateFilter = function () {


                if ($scope.selectedFrom != undefined && $scope.selectedTill != undefined) {
                    $scope.page_number = 0;
                    $scope.GetPatientInformation();

                }


            }
            $scope.GetInformation = function () {
                $scope.manageNextPrevious();
                $scope.viewType = $scope.SHOW_CASE_PAPER;
                $scope.GetPatientInformation(-1, -1);
                $scope.GetTreatment();
                $scope.GetDepartment();
                $scope.GetDoctors();

            }

            $scope.removeDateFilter = function () {
                $scope.selectedFrom = undefined;
                $scope.selectedTill = undefined;
                $scope.fromDate = undefined;
                $scope.till = undefined;
                $scope.page_number = 0;
                $scope.GetPatientInformation();
            }
            $scope.convertDate = function ($dateString) {

                return new Date($dateString);
            };


            $scope.fetchNext = function () {
                if ($scope.isFurtherFetchNeeded) {
                    $scope.page_number++;
                    $scope.GetPatientInformation();
                }
            }
            $scope.fetchPrevious = function () {
                if ($scope.page_number > 0) {
                    $scope.page_number--;
                    $scope.isFurtherFetchNeeded = true;
                    $scope.GetPatientInformation();
                }
            }

            $scope.manageNextPrevious = function () {
                if ($scope.page_number == 0) {
                    $scope.hasPrevious = false;
                }
            }
        });
    </script>
</head>

<body>

<div ng-app="ShowPatients" ng-controller="ShowPatientsController" data-ng-init="GetInformation()">

    <div class="row margin-class">

        <div class="col-lg-2 col-lg-offset-2 alert-info">
            <h3> Total business
                <div>{{business}}</div>
            </h3>
        </div>

        <div class="col-lg-2 alert-success">
            <h3> Received
                <div>{{collection}}</div>
            </h3>
        </div>

        <div class="col-lg-2 alert-danger">

            <h3>
                Balance
                <div>{{business-collection}}</div>
            </h3>
        </div>

        <div class="col-lg-2 alert-warning">

            <h3>
                subsidy to pay
                <div>{{actual_fees-subsidised_fees}}</div>
            </h3>
        </div>
    </div>


    <div class="row">
        <div class="col-lg-10 col-lg-offset-1 small-padding-class">

            <div class="row">
                <div class="col-lg-4">
                    From : <input id="fromDate" type="date" class="form-control"
                                  data-date-format="dd/mm/yyyy" ng-model="fromDate.value"
                                  ng-change="filterFromDate()"> </span>
                </div>


                <div class="col-lg-4">
                    To : <input type="date" id="tillDate" type="date" class="form-control" ng-model="till.value"
                                ng-change="filterTillDate()"
                                placeholder="Till"/>
                </div>

                <div class="col-lg-1 margin-class">
                    <button class="btn btn-primary" ng-click="applyDateFilter()">Apply</button>
                </div>

                <div class="col-lg-1 margin-class">

                    <button class="btn btn-danger" ng-click="removeDateFilter()">Remove date filter</button>
                </div>


            </div>

            <div class="row margin-class">
                <div class="col-lg-2">
                    Filter by department
                    <select class="form-control col-lg-4" ng-model="selectedDepartment"
                            ng-change="filterByDepartment()">

                        <option value={{department}} ng-repeat="department in departmentList">
                            {{department.department_name}}
                    </select>

                    <div>
                        <a href="#" class="alert-danger small-padding-class" ng-click="removeDepartmentFilter()"
                           ng-hide="selectedDepartmentId==undefined">Remove</a>
                    </div>
                </div>

                <div class="col-lg-3">

                    Filter by treatment
                    <select class="form-control col-lg-4" ng-model="selectedTreatment" ng-change="filterByTreatment()">

                        <option value={{treatment}} ng-repeat="treatment in treatmentList">
                            {{treatment.treatment_title}}
                        </option>


                    </select>

                    <div>
                        <a href="#" class="alert-danger small-padding-class" ng-click="removeTreatmentFilter()"
                           ng-hide="selectedTreatmentId==undefined">Remove</a>
                    </div>
                </div>

                <div class="col-lg-3">

                    Filter by doctor
                    <select class="form-control col-lg-4" ng-model="selectedDoctor" ng-change="filterByDoctor()">

                        <option value={{doctor}} ng-repeat="doctor in doctorList">
                            {{doctor.doctor_name}}
                        </option>

                    </select>

                    <div>
                        <a href="#" class="alert-danger small-padding-class" ng-click="removeDoctorFilter()"
                           ng-hide="selectedDoctorId==undefined">Remove</a>
                    </div>
                </div>


                <div class="col-lg-2">
                    Filter by treatment type
                    <select class="form-control col-lg-4" ng-model="selectedTreatmentType"
                            ng-change="filterByTreatmentType()">

                        <option value="0">Normal treatment</option>
                        <option value="1">Subsidized treatment</option>
                    </select>


                    <div>
                        <a href="#" class="alert-danger small-padding-class" ng-click="removeTypeFilter()"
                           ng-hide="selectedTreatmentType==undefined">Remove</a>
                    </div>
                </div>


                <div class="col-lg-2">
                    <input type="button" class="btn btn-info margin-class" ng-show="isFilterActive"
                           value="Remove filters" ng-click="removeAllFilters()">
                </div>


            </div>


            <div class="row">
                <nav>
                    <ul class="pager">
                        <li class="previous" ng-enabled="hasPrevious" ng-click="fetchPrevious()"><a href="#"><span
                                    aria-hidden="true">&larr;</span> Older</a></li>


                        <div class="col-lg-3 col-lg-offset-3">


                            <select ng-model="viewType" class="form-control col-lg-3" ng-change="changeViewType()">
                                <option value="{{SHOW_CASE_PAPER}}" selected="selected">Show case papers</option>
                                <option value="{{SHOW_RECEIPTS}}">Show receipts</option>
                            </select>
                        </div>
                        <li class="next" ng-enabled="hasNext" ng-click="fetchNext()"><a href="#">Newer <span
                                    aria-hidden="true">&rarr;</span></a></li>
                    </ul>
                </nav>
            </div>


            <div class="row">
                <div class="col-lg-11">
                    <table id="casepaperlist" ng-show="viewType==SHOW_CASE_PAPER"
                           class="table table-bordered table-hover table-condensed" border="1">
                        <tr>
                            <th>Case Id</th>
                            <th>Receipt No.</th>
                            <th>Patient name</th>
                            <th>Contact</th>
                            <th>Treatment undertaken</th>
                            <th>Treatment fees</th>
                            <th>Fees paid</th>
                            <th>Fees remaining</th>
                            <th>Treatment fess</th>
                            <th>Subsidy applied</th>
                            <th>Department name</th>
                            <th>Date</th>
                            <th>Doctor name</th>
                            <th>Reference number</th>
                        </tr>

                        <tr ng-repeat="casepaper in casepaperList">
                            <td>{{casepaper.case_paper_id}}</td>
                            <td>{{casepaper.receipt_id}}</td>
                            <td>{{casepaper.patient_name}}</td>
                            <td>{{casepaper.patient_contact}}</td>
                            <td>{{casepaper.treatment_title}}</td>
                            <td>{{casepaper.case_paper_fees}}</td>
                            <td>{{casepaper.case_paper_fees_paid}}</td>
                            <td>{{casepaper.case_paper_fees - casepaper.case_paper_fees_paid}}</td>
                            <td>{{casepaper.treatment_actual_fees}}</td>
                            <td>{{casepaper.treatment_actual_fees - casepaper.treatment_fees}}</td>
                            <td>{{casepaper.department_name}}</td>
                            <td>{{convertDate(casepaper.date_of_admission) | date: 'dd/MM/yyyy'}}</td>
                            <td>{{casepaper.doctor_name}}</td>
                            <td>{{casepaper.free_patient_reference_id}}</td>
                        </tr>
                    </table>

                    <table id="receiptlist" ng-hide="viewType==SHOW_CASE_PAPER"
                           class="table table-bordered table-hover table-condensed" border="1">

                        <tr>
                            <th>Receipt No.</th>
                            <th>Case Id</th>
                            <th>Patient name</th>
                            <th>Treatment undertaken</th>
                            <th>Treatment fees</th>
                            <th>Fees paid</th>
                            <th>Department name</th>
                            <th> Receipt Date</th>
                            <th>Case paper date</th>
                            <th>Doctor name</th>
                            <th>Reference number</th>
                        </tr>

                        <tr ng-repeat="casepaper in casepaperList">
                            <td>{{casepaper.receipt_id}}</td>
                            <td>{{casepaper.case_paper_id}}</td>
                            <td>{{casepaper.patient_name}}</td>

                            <td>{{casepaper.treatment_title}}</td>
                            <td>{{casepaper.case_paper_fees}}</td>
                            <td>{{casepaper.amount_paid}}</td>
                            <td>{{casepaper.department_name}}</td>
                            <td>{{convertDate(casepaper.receipt_date) | date: 'dd/MM/yyyy'}}</td>
                            <td>{{convertDate(casepaper.date_of_admission) | date: 'dd/MM/yyyy'}}</td>
                            <td>{{casepaper.doctor_name}}</td>
                            <td>{{casepaper.free_patient_reference_id}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>


    </div>

</div>


</body>
</html>