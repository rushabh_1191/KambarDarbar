<?php
include "db_connect.php";

$patient_data = json_decode(file_get_contents("php://input"), true);


$patientName = $patient_data["patient_name"];
$gender = $patient_data["gender"];

$contct = $patient_data["contact"];
$doctor_id = $patient_data["doctor_id"];
$doctor_name = $patient_data["doctor_name"];
$patientName = mysqli_real_escape_string($conn, $patientName);
$patient_age = $patient_data["patient_age"];

$treatmentInformation = $patient_data["treatment_info"];

$treatmentId = $treatmentInformation["treatment_id"];

$fees = $treatmentInformation["treatment_fees"];
$payment = $patient_data["amount_paid"];
$treatment_taken = $treatmentInformation["treatment_title"];

$refernceid = "N/A";
$is_free = 0;
if (isset($patient_data["reference_id"])) {
    $refernceid = $patient_data["reference_id"];
    $is_free = 1;

}

$date = date('Y-m-d H:i:s');

$receipt_id = "";


$query = "INSERT INTO patient_information (patient_name,patient_age,date_of_admission,gender,patient_contact) VALUES ( '" . $patientName . "'," . $patient_age . ",'" . $date . "','" . $gender . "','" . $contct . "')";


mysqli_query($conn, $query) or die(mysqli_error($conn));

$patient_id = mysqli_insert_id($conn);

$query = "INSERT INTO case_paper_information (case_paper_date, case_paper_treatment_id, casepaper_patient_id, case_paper_fees, case_paper_fees_paid,case_paper_doctor_id,free_patient_reference_id,is_free) VALUES ('" . $date . "'," . $treatmentId . "," . $patient_id . "," . $fees . " , " . $payment . "," . $doctor_id . ",'" . $refernceid . "'," . $is_free . ");";


mysqli_query($conn, $query) or die(mysqli_error($conn));

$case_paper_id = mysqli_insert_id($conn);

$query = "INSERT INTO receipt_information (case_paper_id,receipt_date,amount_paid) VALUE (" . $case_paper_id . ",' " . $date . "'," . $payment . ")";
mysqli_query($conn, $query) or die(mysqli_error($conn));
$receipt_id = mysqli_insert_id($conn);

$response = array();

$response["case_paper_id"] = $case_paper_id;
$response["treatment_taken"] = $treatment_taken;
$response["case_paper_patient_name"] = $patientName;
$response["case_paper_patient_age"] = $patient_age;
$response["patient_gender"] = $gender;
$response["amount_paid"] = $payment;
$response["treatment_fees"] = $fees;
$response["remaining_amount"] = $fees - $payment;
$response["reference_id"] = $refernceid;
$response["reciept_no"] = $receipt_id;
$response["doctor_name"] = $doctor_name;
$response["receipt_date"] = $date;

mysqli_close($conn);

echo json_encode($response);


?>