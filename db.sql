-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Aug 20, 2015 at 07:39 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `kambar_darbar`
--

-- --------------------------------------------------------

--
-- Table structure for table `case_paper_information`
--

CREATE TABLE `case_paper_information` (
  `case_paper_id` int(11) NOT NULL,
  `case_paper_date` datetime NOT NULL,
  `case_paper_treatment_id` int(11) NOT NULL,
  `casepaper_patient_id` int(11) NOT NULL,
  `case_paper_fees` float NOT NULL,
  `case_paper_fees_paid` float NOT NULL,
  `case_paper_doctor_id` int(11) NOT NULL,
  `free_patient_reference_id` varchar(20) NOT NULL,
  `is_free` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `case_paper_information`
--

INSERT INTO `case_paper_information` (`case_paper_id`, `case_paper_date`, `case_paper_treatment_id`, `casepaper_patient_id`, `case_paper_fees`, `case_paper_fees_paid`, `case_paper_doctor_id`, `free_patient_reference_id`, `is_free`) VALUES
  (1, '2015-08-17 13:52:36', 1, 1, 100, 100, 2, 'N/A', 0),
  (2, '2015-08-17 13:57:46', 4, 2, 20, 20, 1, 'N/A', 0),
  (3, '2015-08-17 14:02:34', 1, 3, 1000, 1000, 2, 'N/A', 0),
  (4, '2015-08-17 19:49:14', 2, 4, 250, 250, 2, 'N/A', 0),
  (5, '2015-08-17 19:50:10', 1, 5, 200, 200, 2, 'N/A', 0),
  (6, '2015-08-17 19:51:47', 4, 6, 20, 20, 1, 'N/A', 0),
  (7, '2015-08-17 20:09:24', 1, 7, 2000, 2000, 2, 'N/A', 0),
  (8, '2015-08-17 20:10:54', 2, 8, 250, 250, 2, 'N/A', 0),
  (9, '2015-08-17 20:11:56', 1, 9, 100, 100, 2, 'N/A', 0),
  (10, '2015-08-17 20:14:57', 3, 10, 10000, 10000, 2, 'N/A', 0),
  (11, '2015-08-17 20:21:44', 1, 11, 10, 31, 2, 'N/A', 0),
  (12, '2015-08-17 20:22:27', 4, 12, 20, 20, 1, 'N/A', 0),
  (13, '2015-08-17 20:24:25', 4, 13, 20, 20, 1, 'N/A', 0),
  (14, '2015-08-17 20:25:13', 3, 14, 10000, 10000, 2, 'N/A', 0),
  (15, '2015-08-17 20:26:00', 1, 15, 120, 23, 2, 'N/A', 0),
  (16, '2015-08-17 20:30:56', 3, 16, 10000, 10000, 2, 'N/A', 0),
  (17, '2015-08-17 20:32:33', 2, 17, 250, 250, 2, 'N/A', 0),
  (18, '2015-08-17 20:34:13', 4, 18, 20, 20, 1, 'N/A', 0),
  (19, '2015-08-17 20:35:11', 1, 19, 88, 7, 2, 'N/A', 0),
  (20, '2015-08-17 20:37:30', 1, 20, 100, 22, 2, 'N/A', 0),
  (21, '2015-08-17 20:38:24', 1, 21, 234, 23, 2, 'N/A', 0),
  (22, '2015-08-17 20:39:42', 1, 22, 1000, 500, 2, 'N/A', 0),
  (23, '2015-08-17 20:41:08', 1, 23, 120, 12, 2, 'N/A', 0),
  (24, '2015-08-17 20:41:43', 1, 24, 120, 23, 2, 'N/A', 0),
  (25, '2015-08-17 20:42:54', 3, 25, 10000, 10000, 2, 'N/A', 0),
  (26, '2015-08-17 20:43:46', 3, 26, 10000, 10000, 2, 'N/A', 0);

-- --------------------------------------------------------

--
-- Table structure for table `department_information`
--

CREATE TABLE `department_information` (
  `department_id` int(11) NOT NULL,
  `department_name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_information`
--

INSERT INTO `department_information` (`department_id`, `department_name`) VALUES
  (3, 'Dental'),
  (4, 'Pathology');

-- --------------------------------------------------------

--
-- Table structure for table `doctor_information`
--

CREATE TABLE `doctor_information` (
  `doctor_id` int(11) NOT NULL,
  `doctor_name` varchar(200) NOT NULL,
  `doctor_department_id` int(11) NOT NULL,
  `doctor_contact` varchar(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctor_information`
--

INSERT INTO `doctor_information` (`doctor_id`, `doctor_name`, `doctor_department_id`, `doctor_contact`) VALUES
  (1, 'Maitri Jani', 4, '123123'),
  (2, 'Krupa', 3, '12312312');

-- --------------------------------------------------------

--
-- Table structure for table `patient_information`
--

CREATE TABLE `patient_information` (
  `patient_id` int(11) NOT NULL,
  `patient_name` varchar(100) NOT NULL,
  `patient_contact` varchar(10) NOT NULL,
  `patient_age` int(3) NOT NULL,
  `gender` varchar(6) NOT NULL,
  `date_of_admission` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patient_information`
--

INSERT INTO `patient_information` (`patient_id`, `patient_name`, `patient_contact`, `patient_age`, `gender`, `date_of_admission`) VALUES
  (1, 'Rushabh', '332323', 43, 'Male', '2015-08-17 13:52:36'),
  (2, 'Rushabh', '299', 23, 'Male', '2015-08-17 13:57:46'),
  (3, 'ABC', '3234234', 23, 'Male', '2015-08-17 14:02:34'),
  (4, 'Check print', '123', 12, 'Male', '2015-08-17 19:49:14'),
  (5, 'Ris', '324', 12, 'Male', '2015-08-17 19:50:10'),
  (6, '234', '12', 23, 'Male', '2015-08-17 19:51:47'),
  (7, 'Rushabh', '234234', 12, 'Male', '2015-08-17 20:09:24'),
  (8, '2323', '23232', 12, 'Male', '2015-08-17 20:10:54'),
  (9, '23', '234234', 34, 'Male', '2015-08-17 20:11:56'),
  (10, '32', '234234234', 12, 'Male', '2015-08-17 20:14:57'),
  (11, '23', '123123', 12, 'Male', '2015-08-17 20:21:44'),
  (12, '23', '123', 32, 'Male', '2015-08-17 20:22:27'),
  (13, '324234', '234234234', 12, 'Male', '2015-08-17 20:24:25'),
  (14, '12', '2323', 12, 'Male', '2015-08-17 20:25:13'),
  (15, '324234', '2342', 23, 'Male', '2015-08-17 20:26:00'),
  (16, 'yfh', '86869', 57, 'Male', '2015-08-17 20:30:56'),
  (17, 'fmn', '7657587', 56, 'Male', '2015-08-17 20:32:33'),
  (18, 'jkh', '7578', 67, 'Male', '2015-08-17 20:34:13'),
  (19, 'kjg', '6747', 76, 'Male', '2015-08-17 20:35:11'),
  (20, 'ddfsdf', '324234', 21, 'Male', '2015-08-17 20:37:30'),
  (21, '234234', '23423', 23, 'Male', '2015-08-17 20:38:24'),
  (22, '23', '234234', 23, 'Male', '2015-08-17 20:39:42'),
  (23, 'asasd', '234234', 12, 'Male', '2015-08-17 20:41:08'),
  (24, '234', '123123', 2, 'Male', '2015-08-17 20:41:43'),
  (25, '123324234', '234234', 12, 'Male', '2015-08-17 20:42:54'),
  (26, '123', '123123', 12, 'Male', '2015-08-17 20:43:46');

-- --------------------------------------------------------

--
-- Table structure for table `receipt_information`
--

CREATE TABLE `receipt_information` (
  `receipt_id` int(11) NOT NULL,
  `case_paper_id` int(11) NOT NULL,
  `receipt_date` datetime NOT NULL,
  `amount_paid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `receipt_information`
--

INSERT INTO `receipt_information` (`receipt_id`, `case_paper_id`, `receipt_date`, `amount_paid`) VALUES
  (1, 1, '2015-08-17 13:52:36', 12),
  (2, 1, '2015-08-17 13:54:54', 88),
  (3, 2, '2015-08-17 13:57:46', 20),
  (4, 3, '2015-08-17 14:02:34', 500),
  (5, 3, '2015-08-17 14:02:49', 250),
  (6, 3, '2015-08-17 14:02:57', 250),
  (7, 4, '2015-08-17 19:49:14', 250),
  (8, 5, '2015-08-17 19:50:10', 120),
  (9, 6, '2015-08-17 19:51:47', 20),
  (10, 7, '2015-08-17 20:09:24', 1220),
  (11, 8, '2015-08-17 20:10:54', 250),
  (12, 9, '2015-08-17 20:11:56', 50),
  (13, 9, '2015-08-17 20:13:44', 50),
  (14, 10, '2015-08-17 20:14:57', 10000),
  (15, 7, '2015-08-17 20:18:10', 780),
  (16, 5, '2015-08-17 20:19:19', 40),
  (17, 5, '2015-08-17 20:19:48', 40),
  (18, 11, '2015-08-17 20:21:44', 31),
  (19, 12, '2015-08-17 20:22:27', 20),
  (20, 13, '2015-08-17 20:24:25', 20),
  (21, 14, '2015-08-17 20:25:13', 10000),
  (22, 15, '2015-08-17 20:26:00', 23),
  (23, 16, '2015-08-17 20:30:56', 10000),
  (24, 17, '2015-08-17 20:32:33', 250),
  (25, 18, '2015-08-17 20:34:13', 20),
  (26, 19, '2015-08-17 20:35:11', 7),
  (27, 20, '2015-08-17 20:37:30', 22),
  (28, 21, '2015-08-17 20:38:24', 23),
  (29, 22, '2015-08-17 20:39:42', 500),
  (30, 23, '2015-08-17 20:41:08', 12),
  (31, 24, '2015-08-17 20:41:43', 23),
  (32, 25, '2015-08-17 20:42:54', 10000),
  (33, 26, '2015-08-17 20:43:46', 10000);

-- --------------------------------------------------------

--
-- Table structure for table `treatment_information`
--

CREATE TABLE `treatment_information` (
  `treatment_id` int(11) NOT NULL,
  `treatment_title` varchar(500) NOT NULL,
  `treatment_description` varchar(20000) NOT NULL,
  `treatment_department_id` int(11) NOT NULL,
  `treatment_fees` float NOT NULL,
  `treatment_actual_fees` int(4) NOT NULL,
  `is_rate_predefined` tinyint(1) NOT NULL DEFAULT '1',
  `is_rate_subsidized` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `treatment_information`
--

INSERT INTO `treatment_information` (`treatment_id`, `treatment_title`, `treatment_description`, `treatment_department_id`, `treatment_fees`, `treatment_actual_fees`, `is_rate_predefined`, `is_rate_subsidized`) VALUES
  (1, 'Denture', '', 3, 0, 0, 0, 0),
  (2, 'Root canal', '', 3, 250, 250, 1, 0),
  (3, 'Braces', '', 3, 10000, 10000, 1, 0),
  (4, 'CBC', '', 4, 20, 35, 1, 1),
  (5, 'Lever count', '', 4, 30, 70, 1, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `case_paper_information`
--
ALTER TABLE `case_paper_information`
ADD PRIMARY KEY (`case_paper_id`);

--
-- Indexes for table `department_information`
--
ALTER TABLE `department_information`
ADD PRIMARY KEY (`department_id`);

--
-- Indexes for table `doctor_information`
--
ALTER TABLE `doctor_information`
ADD PRIMARY KEY (`doctor_id`);

--
-- Indexes for table `patient_information`
--
ALTER TABLE `patient_information`
ADD PRIMARY KEY (`patient_id`);

--
-- Indexes for table `receipt_information`
--
ALTER TABLE `receipt_information`
ADD PRIMARY KEY (`receipt_id`);

--
-- Indexes for table `treatment_information`
--
ALTER TABLE `treatment_information`
ADD PRIMARY KEY (`treatment_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `case_paper_information`
--
ALTER TABLE `case_paper_information`
MODIFY `case_paper_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `department_information`
--
ALTER TABLE `department_information`
MODIFY `department_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `doctor_information`
--
ALTER TABLE `doctor_information`
MODIFY `doctor_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `patient_information`
--
ALTER TABLE `patient_information`
MODIFY `patient_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `receipt_information`
--
ALTER TABLE `receipt_information`
MODIFY `receipt_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `treatment_information`
--
ALTER TABLE `treatment_information`
MODIFY `treatment_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;