<?php

include "db_connect.php";
$case_paper_info = json_decode(file_get_contents("php://input"), true);

$case_paper_id = $case_paper_info["case_paper_no"];


$query = "SELECT * FROM receipt_information r INNER JOIN case_paper_information c
ON r.case_paper_id=c.case_paper_id
INNER JOIN doctor_information d
ON d.doctor_id =c.case_paper_doctor_id
INNER JOIN treatment_information t
ON t.treatment_id=c.case_paper_treatment_id
INNER JOIN patient_information p
ON c.casepaper_patient_id=p.patient_id

WHERE c.case_paper_id=" . $case_paper_id;

$result=mysqli_query($conn,$query);

$response=array();
if($result->num_rows==0)
{
    $response["status"]="error";
    $response["message"]="No such case paper present";
}
else{
    $response["status"]="success";
    $response["message"]="fetched data";

    $data=array();
    while($row=$result->fetch_assoc()){
        foreach ($row as $key => $value) {
            $data[$key] = $value;
        }
    }

    $response["data"]=$data;

}

echo json_encode($response);
mysqli_close($conn);

?>