<?php
include "login_verify.php";
?>

<!DOCTYPE html>
<html ng-app="addPatient">


<head lang="en">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Kambar darbar</title>

    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ui-bootstrap-tpls-0.13.0.min.js"></script>

    <link href="css/bootstrap.min.css" rel="stylesheet"/>

    <link href="css/style.css" rel="stylesheet"/>
    <style>
        @media print {
            body * {
                visibility: hidden;
            }

            .modal-header{
                visibility: hidden;
            }


            #print-content * {
                visibility: visible;
                width: 100%;
                max-width: 960px;
            }

            .modal {
                position: absolute;
                left: 0;
                top: 0;
                margin: 0;
                padding: 0;
                min-height: 550px
            }
    </style>

    <script>
        var app = angular.module('addPatient', ['ui.bootstrap']);


        app.controller('addPatientController', function ($scope, $http, $modal) {

            $scope.convertDate = function ($dateString) {

                console.log($dateString)
                return new Date($dateString);
            };

            $scope.addPatient = function () {


                 $scope.isTreatmentInvalid = ($scope.selectedTreatment == undefined)

                 if ($scope.isTreatmentInvalid)
                 return;

                 $scope.isUsernameInvalid = ($scope.firstName == undefined)
                 if ($scope.isUsernameInvalid)
                 return;

                 $scope.isAgeInvalid = ($scope.age == undefined | $scope.age == NaN | $scope.age > 99)
                 if ($scope.isAgeInvalid)
                 return;


                 $scope.isAmountInvalid = ( $scope.amount_paid == undefined)
                 if ($scope.isAmountInvalid)
                 return;

                 $scope.isContactInvalid = ($scope.contact == undefined || $scope.contact == NaN || $scope.contact.length < 10)
                 if ($scope.isContactInvalid)
                 return;

                 if ($scope.isFree) {
                 if ($scope.reference_id == undefined) {
                 alert("Please enter reference id");
                 return;
                 }
                 }

                 $scope.isGenderInvalid = ($scope.gender == undefined)
                 if ($scope.isGenderInvalid) {
                 return;
                 }

                 if (!$scope.isRateDefined) {
                 $scope.isTreatmentFeesNotProvided = ($scope.treatment_fees == undefined)
                 if ($scope.isTreatmentFeesNotProvided) {
                 return;
                 }
                 }

                 $scope.selectedTreatment = JSON.parse($scope.selectedTreatment);
                 if (!$scope.isRateDefined) {

                 $scope.selectedTreatment.treatment_fees = $scope.treatment_fees;
                 }
                 $scope.data = {
                 "patient_name": $scope.firstName,
                 "patient_age": $scope.age,
                 "amount_paid": $scope.amount_paid,
                 "gender": $scope.gender,
                 "contact": $scope.contact,
                 "treatment_info": $scope.selectedTreatment,
                 "reference_id": $scope.reference_id,
                 "doctor_id": JSON.parse($scope.selectedDoctor).doctor_id,
                 "doctor_name": JSON.parse($scope.selectedDoctor).doctor_name
                 };

                $scope.formData = {"form_data": $scope.data};


                $http.post('add_patients.php', $scope.data).success(function (response) {

                                     console.log(response)


                 $scope.firstName = undefined;
                 $scope.age = undefined;
                 $scope.amount_paid = undefined;
                 $scope.contact = undefined;
                 $scope.selectedTreatment = undefined;
                 $scope.isRateDefined = true;


                 $scope.opendModal(response);

                 });

            }

            $scope.opendModal = function ($response) {


                var casePaperModel = $modal.open({
                    animation: true,
                    templateUrl: 'case_paper.html',
                    controller: 'case_paper_controller',

                    resolve: {
                        items: function () {
                            return $response;
                        }
                    }
                });

                casePaperModel.result.then(function (selectedItem) {

                }, function () {

                });
            }
            $scope.update = function () {
                $scope.treatmentJSON = JSON.parse($scope.selectedTreatment);

                $scope.manageAmount();

                $scope.departmentInfo = {"department_id": $scope.treatmentJSON.department_id};

                $scope.isRateDefined = $scope.treatmentJSON.is_rate_predefined == 1;

                $http.post("GetDoctors.php", $scope.departmentInfo).success(function (data) {
                    $scope.doctorList = data.data;
                });


            }

            $scope.manageAmount = function () {
                $scope.treatmentJSON = JSON.parse($scope.selectedTreatment);

                if (!$scope.isFree) {
                    if ($scope.isRateDefined) {
                        console.log($scope.treatmentJSON.treatment_fees)
                        $scope.amount_paid = parseInt($scope.treatmentJSON.treatment_fees);
                    }

                }
                else {
                    $scope.amount_paid = 0;
                }

                console.log(typeof ($scope.amount_paid))
            }


            $scope.updateDoctor = function () {

            }


            $scope.updateCasePaper = function () {


                $scope.updateFormData = {
                    "case_paper_id": $scope.cp_case_paper_id,
                    "case_paper_paid_amount": $scope.cp_current_amount_paid,
                    "patient_age": $scope.cp_patient_age,
                    "patient_name": $scope.cp_patient_name,
                    "gender": $scope.cp_patient_gender,
                    "treatment_taken": $scope.cp_treatment_title,
                    "treatment_fees": $scope.cp_treatment_fees,
                    "prev_paid": $scope.cp_amount_paid,
                    "doctor_name": $scope.cp_doctor_name
                }

                $http.post("update_case_paper_info.php", $scope.updateFormData).success(function (data) {
                    $scope.opendModal(data);

                });
            }


            $scope.showCasepaper = function () {

                window.open("ShowCasePaperInformation.php");
            };

            $scope.manage = function () {

                window.open("ManageTrust.php");
            };
            $scope.init = function () {
                $http.get("GetTreatment.php").success(function (content) {
                    $scope.treatmentList = JSON.parse(angular.toJson(content));
                    $scope.isRateDefined = true;
                    $scope.isAddPatient = true;
                    $scope.manageButtonValue();

                });

            };

            $scope.changeView = function () {
                $scope.isAddPatient = !$scope.isAddPatient;
                $scope.manageButtonValue();
            }

            $scope.manageButtonValue = function () {
                if ($scope.isAddPatient) {
                    $scope.toggle_button_value = "Search case paper"
                }
                else {
                    $scope.toggle_button_value = "Enter case paper"
                }
            }

            $scope.fetchCasePaper = function () {
                $scope.data = {"case_paper_no": $scope.case_paper_no}

                $http.post("search_case_paper.php", $scope.data).success(function (case_paper_information) {

                    console.log(case_paper_information);
                    var status = case_paper_information.status;
                    if (status == "error") {
                        $scope.case_paper_error = case_paper_information.message;
                        console.log($scope.case_paper_error);
                        $scope.hasError = true;

                    }
                    else {
                        $scope.hasError = false;


                        $scope.cp_search_data = case_paper_information.data;

                        $scope.cp_case_paper_id = $scope.cp_search_data.case_paper_id;
                        $scope.cp_doctor_name = $scope.cp_search_data.doctor_name;
                        $scope.cp_doctor_contact = $scope.cp_search_data.doctor_contact;

                        $scope.cp_treatment_title = $scope.cp_search_data.treatment_title;
                        $scope.cp_treatment_fees = $scope.cp_search_data.case_paper_fees;

                        $scope.cp_patient_name = $scope.cp_search_data.patient_name;
                        $scope.cp_patient_age = $scope.cp_search_data.patient_age;
                        $scope.cp_patient_gender = $scope.cp_search_data.gender;

                        $scope.cp_amount_paid = parseInt($scope.cp_search_data.case_paper_fees_paid);
                        $scope.cp_case_paper_date = $scope.cp_search_data.case_paper_date;
                        $scope.cp_prev_receipt_id = $scope.cp_search_data.receipt_id;
                        $scope.cp_balance = ($scope.cp_treatment_fees - $scope.cp_amount_paid);
                        $scope.cp_prev_receipt_date = new Date($scope.cp_search_data.receipt_date);
                        $scope.cp_refrence_id = $scope.cp_search_data.free_patient_reference_id;

                        $scope.cp_is_free = $scope.cp_search_data.is_free;

                        if ($scope.cp_balance > 0) {
                            if ($scope.cp_is_free == 1) {
                                $scope.isAmountPending = false;
                                $scope.payment_meta = "Free patient"
                            }
                            else {
                                $scope.isAmountPending = true;
                            }

                        }
                        else {

                            $scope.isAmountPending = false;
                            $scope.payment_meta = "No balance amount to be paid"
                        }
                    }

                    $scope.hasData = !$scope.hasError;
                });
            }

            $scope.logout = function () {
                $scope.logoutInfor = {}
                $http.post("logout.php", $scope.logoutInfor).success(function () {
                    window.location.reload(true);
                });
            }

        })
        ;


        app.controller('case_paper_controller', function ($scope, $modalInstance, items) {


            $scope.cp_id = items.case_paper_id;

            $scope.reciept_no = items.reciept_no;
            $scope.cp_name = items.case_paper_patient_name;
            $scope.cp_age = items.case_paper_patient_age;
            $scope.cp_treatment_title = items.treatment_taken;
            $scope.cp_treatment_amount = items.treatment_amout
            $scope.cp_paid_amount = items.amount_paid;
            $scope.cp_remaining_amount = items.remaining_amount;
            $scope.reference_id = items.reference_id;
            $scope.doctor_name = items.doctor_name;
            $scope.cp_treatment_fees = items.treatment_fees;
            $scope.cp_receipt_date = items.receipt_date;

            $scope.convertDate = function ($dateString) {

                console.log($dateString)
                return new Date($dateString);
            };

            $scope.ok = function () {
                $modalInstance.close(items);
                window.print();
                window.location.reload(true);

            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
                window.location.reload(true);
            };
        });
    </script>


</head>
<body>

<?php
include "db_connect.php";
?>
<div ng-controller="addPatientController" data-ng-init="init()" class="container-fluid">


    <script type="text/ng-template" id="case_paper.html">
        <div id="print-content">
            <div class="modal-header">
                <h4 class="modal-title">Kambar Darbar</h4>

            </div>
            <div class="modal-body">
                <div class="panel panel-primary margin-class">
                    <div class="panel-heading">
                        <h4>Reciept No : {{reciept_no}}</h4>
                    </div>


                    <div class="clearfix">

                        <span style="text-align:left; float: left;" class="small-margin-class">
                            Case Paper : {{cp_id}}
                        </span>
                        <span class="small-margin-class" style="text-align:right; float: right;"><strong>Treated by : Dr.{{doctor_name}}</strong></span>

                    </div>

                    <div class="clearfix">
                        <div style="text-align:left; float: left;" class="small-margin-class">
                            Name : {{cp_name}}
                        </div>

                        <span class="small-margin-class" style="text-align:right; float: right;">
                                <strong> <i><u>Reference number : {{reference_id}}<u></i></strong></span>

                    </div>

                    <div class="clearfix">
                        <div style="text-align:left;float: left;" class="small-margin-class">
                            Age : {{cp_age}}
                            
                        </div>
                        <span align="right" class="small-margin-class" style="text-align:right; float: right;">
                                 <i>Fees : Rs.{{cp_treatment_fees}}</i></span>

                        <span>Date : {{convertDate(cp_receipt_date) | date: 'dd/MM/yyyy'}}</span>

                    </div>


                    <div class="row">
                        <div class="col-lg-12 small-margin-class">
                            <table class="table table-bordered table-content">

                                <tr>
                                    <th>
                                        Treatment taken
                                    </th>
                                    <th>
                                        Amount paid
                                    </th>
                                </tr>

                                <tr>
                                    <td>{{cp_treatment_title}}</td>
                                    <td>{{cp_paid_amount}}</td>

                                <tr>
                                    <td align="right">Remaining Amount</td>
                                    <td>{{cp_remaining_amount}}</td>
                                </tr>


                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="ok()">Print</button>
            <button type="button" class="btn btn-default" ng-click="cancel()">Close</button>
        </div>
    </script>


    <form id="patient_information">
        <div ng-bind="case_paper_id">123</div>
        <div class="row">
            <div class="col-lg-7 col-lg-offset-3">

                <div class="panel panel-primary margin-class">

                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-lg-4">
                                <span><h3>Kambar Darbar</h3></span>
                            </div>
                            <div class="col-lg-2 col-lg-offset-5">
                                <input align="right" class="margin-class form-control btn-danger" type="button"
                                       value="Logout" ng-click="logout()">
                            </div>
                        </div>


                        <input type="button" class="btn-danger margin-class" value="{{toggle_button_value}}"
                               ng-click="changeView()" align="right">


                    </div>


                    <div class="panel-body" ng-show="isAddPatient">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="margin-class">
                                    <p class="text-info"> Select Treatment</p>


                                    <select name="treatment" id="treatment" ng-change="update()"
                                            ng-model="selectedTreatment"
                                            class="form-control">
                                        <option value={{treatment}} ng-repeat="treatment in treatmentList">
                                            {{treatment.treatment_title}}
                                        </option>
                                    </select>


                        <span style="color:red" ng-show="isTreatmentInvalid">
                        <span>Please select treatment</span>
                        </span>
                                </div>


                            </div>


                        </div>

                        <div class="input-group margin-class">
                            <span class="input-group-addon">Dr.</span>
                            <select name="doctor" class="form-control" ng-model="selectedDoctor"
                                    ng-change="updateDoctor()">
                                <option value="{{doctor}}" ng-repeat="doctor in doctorList">{{doctor.doctor_name}}
                                </option>
                            </select>
                        </div>

                        <div class="margin-class">
                            <input type="text" placeholder="Patient name" class="form-control" id="patient_name"
                                   ng-model="firstName"/>

                        <span style="color:red" ng-show="isUsernameInvalid">
                        <span>Enter patient name</span>
                        </span>
                        </div>

                        <div class="row">
                            <div class="col-lg-2 margin-class">

                                <input type="number" placeholder="Age" maxlength="3" id="patient_age"
                                       class="form-control"
                                       ng-model="age"/>

                            <span style="color:red">
                            <span ng-show="isAgeInvalid">Please provide patient Age</span>
                        </span>

                            </div>
                            <div class="col-lg-4 margin-class">

                                <div class="input-group ">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type="number" placeholder="Amount paid" class="form-control"
                                           ng-model="amount_paid">

                                </div>

                            <span style="color:red" ng-show="isAmountInvalid">
                            <span>Please enter amount</span>
                            </span>
                            </div>

                            <div class="col-lg-4 margin-class">

                                <div class="input-group ">
                                    <span class="input-group-addon">Rs.</span>
                                    <input type="number" placeholder="Treatment fees" class="form-control"
                                           ng-readonly="isRateDefined"
                                           ng-model="treatment_fees">

                                </div>

                                <p class="alert-danger small-padding-class" ng-show="!isRateDefined">
                                    <strong>Enter treatment fees</strong></p>

                                <p class="alert-danger padding-class" ng-show="isTreatmentFeesNotProvided">
                                    <strong>Please give treatment fees</strong></p>
                                </span>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-5 margin-class">

                                <input type="number" placeholder="Contact number" class="form-control" maxlength="10"
                                       ng-model="contact"/>

                            <span style="color:red" ng-show="isContactInvalid">
                            <span>Please provide contact</span>
                            </span>
                            </div>
                            <div class="col-lg-6 small-margin-class">


                                <div class="radio">
                                    <label>
                                        <input type="radio" name="optionsRadios" ng-model="gender" id="optionsRadios1"
                                               value="Male" checked>
                                        Male
                                    </label>


                                    <label>
                                        <input type="radio" name="optionsRadios" ng-model="gender" id="optionsRadios2"
                                               value="Female">
                                        Female
                                    </label>

                                </div>
                            <span style="color:red" ng-show="isGenderInvalid">
                            <span>Please specify gender</span>
                            </span>
                            </div>


                        </div>

                        <div class="row">
                            <div class="checkbox margin-class col-lg-7">
                                <p>
                                    <label>
                                        <input type="checkbox" ng-model="isFree" ng-change="manageAmount()"> Free
                                        patient
                                    </label>

                                    <input type="text" class="form-control" ng-model="reference_id"
                                           placeholder="Reference id" ng-readonly="!isFree"/>
                                </p>

                                <p class="alert-danger padding-class" ng-show="isFree"><strong>Note : This patient will
                                        not
                                        be charged</strong></p>
                            </div>


                        </div>
                        <div class="margin-class">
                            <input type="button" value="Save & Print" ng-click="addPatient()"
                                   class="form-control btn btn-primary">
                        </div>

                    </div>
                    <div class="panel-body" ng-hide="isAddPatient">
                        <div class="row margin-class">

                            <div class="col-lg-5 col-lg-offset-2">
                                <input type="number" class="form-control" ng-model="case_paper_no"
                                       placeholder="Enter case paper number">

                                <span class="alert-danger small-padding-class" ng-bind="case_paper_error"
                                      ng-show="hasError"></span>
                            </div>
                            <div class="col-lg-2">
                                <input type="button" ng-click="fetchCasePaper()" class="form-control btn-info"
                                       value="Search">
                            </div>
                        </div>
                        <div class="row margin-class" ng-show="hasData">

                            <div class="row">

                                <div class="row panel-success margin-class">
                                    <div class="panel-heading">
                                        Patient information
                                    </div>
                                    <div class="row panel-body">
                                        <div class="col-lg-4">
                                            Patient name : <span ng-bind="cp_patient_name"></span>
                                        </div>

                                        <div class="col-lg-4">
                                            Age : <span ng-bind="cp_patient_age"></span>
                                        </div>
                                        <div class="col-lg-4">
                                            Gender : <span ng-bind="cp_patient_gender"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row panel-info margin-class">
                                    <div class="panel-heading">
                                        Treatment information
                                    </div>

                                    <div class="panel-body">
                                        <div class="col-lg-5">
                                            Treatment undertaken :
                                            <span ng-bind="cp_treatment_title"></span>
                                        </div>

                                        <div class="col-lg-5">
                                            Treatment fees :
                                            <span ng-bind="cp_treatment_fees"></span>
                                        </div>
                                    </div>
                                </div>


                                <div class="row panel-warning margin-class">
                                    <div class="panel-heading">
                                        Doctor information
                                    </div>

                                    <div class="panel-body">
                                        <div class="col-lg-5">
                                            Treated by Dr.
                                            <span ng-bind="cp_doctor_name"></span>
                                        </div>

                                        <div class="col-lg-5">
                                            Doctor contact :
                                            <span ng-bind="cp_doctor_contact"></span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="panel-danger">

                                        <div class="panel-heading">
                                            Payment information
                                        </div>

                                        <div class="row panel-body small-margin-class">

                                            <div class="row">
                                                <div class="col-lg-2">
                                                    <strong>Paid : <span ng-bind="cp_amount_paid"></span></strong>
                                                </div>


                                                <div class="col-lg-3">
                                                    <strong>Receipt number : <span ng-bind="cp_prev_receipt_id"></span></strong>
                                                </div>

                                                <div class="col-lg-3">
                                                    <strong>On : {{cp_prev_receipt_date| date: 'dd/MM/yyyy'}}</strong>
                                                </div>

                                                <div class="col-lg-3">
                                                    <strong>Balance : {{cp_balance}}</strong>
                                                </div>
                                            </div>
                                            <br/>
                                            <br/>

                                            <div class="row  small-margin-class">
                                                <div class="col-lg-4">
                                                    <b>Free patient Reference id</b> : {{cp_refrence_id}}
                                                </div>

                                                <div class="col-lg-4">
                                                    <input type="number" ng-model="cp_current_amount_paid"
                                                           class="form-control"
                                                           placeholder="Amount paid" ng-readonly="!isAmountPending">

                                                    <div class="alert-success small-padding-class"
                                                         ng-show="!isAmountPending" ng-bind="payment_meta"></div>
                                                </div>

                                                <div class="col-lg-4">
                                                    <input type="button" ng-disabled="cp_current_amount_paid==undefined"
                                                           class="form-control" value="Add"
                                                           ng-click="updateCasePaper()"/>
                                                </div>
                                            </div>


                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-lg-6">
                            <div class="margin-class">
                                <input type="button" value="List casepapers" ng-click="showCasepaper()"
                                       class="form-control btn btn-primary">


                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="margin-class">
                                <input type="button" value="Manage KambarDarbar" ng-click="manage()"
                                       class="form-control btn btn-primary">


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </form>


</body>
</html>