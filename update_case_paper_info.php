<?php
include "db_connect.php";

$case_paper_info=$case_paper_info = json_decode(file_get_contents("php://input"), true);

$case_paper_id=$case_paper_info["case_paper_id"];
$case_paper_paid_amounts=$case_paper_info["case_paper_paid_amount"];


$case_paper_patient_age=$case_paper_info["patient_age"];
$case_paper_patient_name=$case_paper_info["patient_name"];
$case_paper_patient_gender=$case_paper_info["gender"];

$case_paper_treatment_taken=$case_paper_info["treatment_taken"];
$case_paper_treatment_fees=$case_paper_info["treatment_fees"];

$case_paper_prev_paid_fees=$case_paper_info["prev_paid"];
$case_paper_doctor_name=$case_paper_info["doctor_name"];

$referenceId="";

if(isset($case_paper_info["reference_id"])){
    $referenceId=$case_paper_info["reference_id"];
}

$total_paid=$case_paper_paid_amounts+$case_paper_prev_paid_fees;

$query="UPDATE case_paper_information set case_paper_fees_paid=" . $total_paid . " WHERE case_paper_id=" . $case_paper_id;

$result=mysqli_query($conn,$query);
$date = date('Y-m-d H:i:s');

$query="INSERT INTO receipt_information (case_paper_id,receipt_date,amount_paid) VALUE (". $case_paper_id .",' " . $date. "',".$case_paper_paid_amounts.")";

$result=mysqli_query($conn,$query);

$receipt_id=mysqli_insert_id($conn);

$response=array();
$response["case_paper_id"]=$case_paper_id;
$response["treatment_taken"]=$case_paper_treatment_taken;
$response["case_paper_patient_name"]=$case_paper_patient_name;
$response["case_paper_patient_age"]=$case_paper_patient_age;
$response["patient_gender"]=$case_paper_patient_gender;
$response["amount_paid"]=$case_paper_paid_amounts;
$response["treatment_fees"]=$case_paper_treatment_fees;
$response["remaining_amount"]=$case_paper_treatment_fees-$total_paid;
$response["reference_id"]=$referenceId;
$response["reciept_no"]=$receipt_id;
$response["doctor_name"]=$case_paper_doctor_name;

echo json_encode($response);


?>