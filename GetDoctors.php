<?php

include "db_connect.php";

$department_id= json_decode(file_get_contents("php://input"),true);

$addCondition="";
if(isset($department_id["department_id"])){
    $addCondition=" WHERE doctor_department_id=" .$department_id["department_id"];
}
$query="SELECT * FROM doctor_information dr INNER JOIN department_information di ON dr.doctor_department_id = di.department_id " .$addCondition;

$result=mysqli_query($conn,$query) ;
$doctor_data=array();
$doctor_list=array();
while( $row = $result->fetch_assoc())
{
    $data = array();
    foreach ($row as $key => $value) {
        $data[$key] = $value;
    }

    array_push($doctor_list,$data);
}

$doctor_data["data"]=$doctor_list;
echo json_encode($doctor_data);

mysqli_close($conn);

?>

