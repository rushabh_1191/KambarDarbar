<?php
include "login_verify.php";
?>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/html">

<head>

    <title>Manage | KambarDarbar</title>

    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/style.css"/>
    <script src="js/jquery-1.11.3.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/ui-bootstrap-tpls-0.13.0.min.js"></script>

    <script>
        var app = angular.module("manage_trust", ['ui.bootstrap'])

        app.controller("manage_trust_controller", function ($scope, $http, $modal) {

            $scope.selectedDepartmentId = undefined;
            $scope.data = {"department_id": $scope.selectedDepartmentId}
            $scope.initialize = function () {

                $scope.GetDepartment();
                $scope.GetDoctors();
                $scope.GetTreatment();
            }

            $scope.showAll = function () {
                $scope.selectedDepartmentId = undefined;
                $scope.selectedDepartment = undefined;
                $scope.data = undefined;
                $scope.initialize();
            }
            $scope.applyFilter = function () {


                $departmentInfo = JSON.parse($scope.selectedDepartment)
                $scope.selectedDepartmentId = $departmentInfo.department_id

                $scope.data = {"department_id": $scope.selectedDepartmentId}

                $scope.GetDoctors();
                $scope.GetTreatment();
            }
            $scope.GetDoctors = function () {

                $http.post("GetDoctors.php", $scope.data).success(function (content) {
                    $scope.doctorList = content.data;
                });
            };
            $scope.GetTreatment = function () {
                $http.post("GetTreatment.php", $scope.data).success(function (content) {
                    $scope.treatmentList = JSON.parse(angular.toJson(content));
                });
            }
            $scope.GetDepartment = function () {

                $http.post("GetDepartment.php").success(function (content) {
                    $scope.departmentList = content.data;
                });
            };

            $scope.AddTreatment = function () {
                var casePaperModel = $modal.open({
                    animation: true,
                    templateUrl: 'add_treatment.html',
                    controller: 'add_treatment_controller',

                    resolve: {
                        items: function () {
                            return $scope.departmentList;
                        }
                    }
                });

                casePaperModel.result.then(function (selectedItem) {

                }, function () {

                });

            }

            $scope.addDoctor = function () {
                var doctorModel = $modal.open({
                    animation: true,
                    templateUrl: 'add_doctor.html',
                    controller: 'add_doctor_controller',

                    resolve: {
                        items: function () {
                            return $scope.departmentList;
                        }
                    }
                });

                casePaperModel.result.then(function (selectedItem) {

                }, function () {

                });

            }

            $scope.addDepartment = function () {
                console.log("add")
                var departmentModal = $modal.open({
                    animation: true,
                    templateUrl: 'add_department.html',
                    controller: 'add_department_controller',

                    resolve: {
                        items: function () {

                        }
                    }

                });

                departmentModal.result.then(function (selectedItem) {

                }, function () {

                });

            }


        });

        app.controller("add_treatment_controller", function ($scope, $modalInstance, items, $http) {

            $scope.departmentList = items;
            $scope.is_decide_on_ispection=false;
            $scope.is_subsidized=false;

            $scope.resetForm= function () {

                $scope.is_decide_on_ispection=false;
                $scope.is_subsidized=false;
                $scope.selectedDepartment = undefined;
                $scope.treatment = undefined;
                $scope.fees = undefined;
                $scope.actual_fees=undefined;

            }
            $scope.refineValue=function(value){
                if(value){
                    return 1;
                }
                return 0;
            }
            $scope.manageFees=function(){

                if(!$scope.is_subsidized){
                    $scope.actual_fees=$scope.fees;
                }

            }
            $scope.addTreatment = function () {

                $scope.selectedDepartmentId = JSON.parse($scope.selectedDepartment).department_id;

                console.log($scope.is_subisidized);
                $scope.manageFees();
                $scope.data = {
                    "treatment_title": $scope.treatment,
                    "department_id": $scope.selectedDepartmentId,
                    "fees": $scope.fees,
                    "actual_fees":$scope.actual_fees,
                    "is_rate_predefined":$scope.refineValue(!$scope.is_decide_on_ispection),
                    "is_subsidized": $scope.refineValue($scope.is_subsidized)
                }



                $http.post("AddTreatment.php", $scope.data).success(function (content) {


                    $scope.resetForm();

                    if (content.status == "success") {
                        $scope.isSuccess = true;
                        $scope.successMessage = content.message;
                    }
                });
            }

        });

        app.controller("add_doctor_controller", function ($scope, $modalInstance, items, $http) {

            $scope.departmentList = items;

            $scope.addDoctor = function () {
                $scope.selectedDepartmentId = JSON.parse($scope.selectedDepartment).department_id;

                $scope.data = {
                    "doctor_name": $scope.doctor_name,
                    "department_id": $scope.selectedDepartmentId,
                    "contact": $scope.contact
                }

                $http.post("AddDoctor.php", $scope.data).success(function (content) {

                    $scope.selectedDepartment = undefined;
                    $scope.doctor_name = undefined;
                    $scope.contact = undefined;


                    if (content.status == "success") {
                        $scope.isSuccess = true;
                        $scope.successMessage = content.message;
                    }
                });
            };

        });

        app.controller("add_department_controller", function ($scope, $modalInstance, items, $http) {

            $scope.addDepartment = function () {
                $scope.data = {"department_name": $scope.departmentName}

                $http.post("AddDepartment.php", $scope.data).success(function (content) {

                });
            }
        });

    </script>
</head>


<body>

<div class="row margin-class" ng-app="manage_trust" ng-controller="manage_trust_controller" ng-init="initialize()">


    <script type="text/ng-template" id="add_treatment.html">
        <div class="modal-header">
            <h2 class="modal-title">Add treatment</h2>
        </div>
        <div class="modal-body">


            <div class=" row margin-class">
                <div class="col-lg-12">
                    <select class="form-control margin-class" ng-model="selectedDepartment">

                        <option value={{department}} ng-repeat="department in departmentList">
                            {{department.department_name}}
                    </select>
                    <input type="text" placeholder="Treatment" ng-model="treatment" class="form-control margin-class"/>

                    <div class="row margin-class">
                        <div class="col-lg-6">
                            <label>
                                <input type="checkbox" ng-model="is_decide_on_ispection"> Decide on inspection
                            </label>
                        </div>

                        <div class="col-lg-6">
                            <label>
                                <input type="checkbox" ng-model="is_subsidized"> Subsidized treatment
                            </label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <input type="number" class="form-control margin-class" ng-model="fees" ng-readonly="isPredefined" placeholder="Fees"/>
                        </div>

                        <div class="col-lg-6">
                            <input type="number" class="form-control margin-class" ng-model="actual_fees" ng-readonly="!is_subsidized" placeholder="Actual fees"/>
                        </div>
                    </div>
                    <p class="bg-success margin-class small-padding-class" ng-show="isSuccess"
                       ng-bind="successMessage"></p>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="addTreatment()">Add</button>

        </div>
    </script>

    <script type="text/ng-template" id="add_doctor.html">
        <div class="modal-header">
            <h2 class="modal-title">Add treatment</h2>
        </div>
        <div class="modal-body">


            <div class=" row margin-class">
                <div class="col-lg-12">
                    <select class="form-control margin-class" ng-model="selectedDepartment">

                        <option value={{department}} ng-repeat="department in departmentList">
                            {{department.department_name}}
                    </select>
                    <input type="text" placeholder="Doctor name" ng-model="doctor_name"
                           class="form-control margin-class"/>
                    <input type="number" class="form-control margin-class" ng-model="contact"
                           placeholder="Contact number"/>

                    <p class="bg-success margin-class small-padding-class" ng-show="isSuccess"
                       ng-bind="successMessage"></p>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="addDoctor()">Add</button>

        </div>
    </script>

    <script type="text/ng-template" id="add_department.html">
        <div class="modal-header">
            <h2 class="modal-title">Add Department</h2>
        </div>
        <div class="modal-body">


            <div class=" row margin-class">
                <div>
                    <input type="text" placeholder="Department name" ng-model="departmentName"
                           class="form-control margin-class"/>

                    <p class="bg-success margin-class small-padding-class" ng-show="isSuccess"
                       ng-bind="successMessage"></p>
                </div>

            </div>

        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-click="addDepartment()">Add</button>

        </div>
    </script>

    <div class="row">
        <div class="col-lg-1 col-lg-offset-3">
            <strong>Department</strong>
        </div>
        <div class="col-lg-4">
           <span> <select class="form-control col-lg-4" ng-model="selectedDepartment"

                          ng-change="applyFilter()">

                   <option value={{department}} ng-repeat="department in departmentList">
                       {{department.department_name}}
               </select></span>


            <a href="#" ng-click="showAll()" ng-show="(selectedDepartment!=undefined)">Show All</a>


        </div>
        <div class="col-lg-2">
            <input type="button" class="btn btn-primary" ng-click="addDepartment()" value="Add"/>
        </div>
    </div>

    <div class="col-lg-5 col-lg-offset-1">

        <div class="row">
            <div class="col-lg-3">
                <div><span><h3>Treatment</h3></span></div>
            </div>
            <div class="col-lg-1 margin-class">
                <input type="button" class="btn btn-primary" value="Add" ng-click="AddTreatment()"/>
            </div>
        </div>
        <table class="table table-bordered">
            <tr>
                <th>#</th>
                <th>Treatment</th>
                <th>Fees</th>
                <th>Treatment actual fees</th>
            </tr>
            <tr ng-repeat="treatment in treatmentList">
                <td>{{treatment.treatment_id}}</td>
                <td>{{treatment.treatment_title}}</td>
                <td>{{treatment.treatment_fees}}</td>
                <td>{{treatment.treatment_actual_fees}}</td>

            </tr>
        </table>
    </div>

    <div class="col-lg-5">

        <div class="row">
            <div class="col-lg-2">
                <div><span><h3>Doctors</h3></span></div>
            </div>
            <div class="col-lg-1 margin-class">
                <input type="button" class="btn btn-primary" value="Add" ng-click="addDoctor()"/>
            </div>
        </div>
        <table class="table table-bordered">
            <tr>

                <th>Dotor name</th>
                <th>Doctor department</th>
                <th>Doctor contact</th>
            </tr>
            <tr ng-repeat="doctor in doctorList">
                <td>Dr. {{doctor.doctor_name}}</td>
                <td>{{doctor.department_name}}</td>
                <td>{{doctor.doctor_contact}}</td>

            </tr>
        </table>
    </div>

</div>

</body>

</html>
