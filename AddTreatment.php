
<?php

include "db_connect.php";
$treatment_information =json_decode(file_get_contents("php://input"),true);

$treatmentTitle=mysqli_real_escape_string($conn,$treatment_information["treatment_title"]);
$treatment_department=$treatment_information["department_id"];

$treatment_rate_predefined=$treatment_information["is_rate_predefined"];
$actual_fees=0;
$treatment_fees=0;

if($treatment_rate_predefined==true){
    $treatment_fees=$treatment_information["fees"];
    $actual_fees=$treatment_fees;
}

if(isset($treatment_information["actual_fees"])){
    $actual_fees=$treatment_information["actual_fees"];
}

$treatment_subsidized=$treatment_information["is_subsidized"];

$query="insert into treatment_information (treatment_title,treatment_department_id,treatment_fees,
treatment_actual_fees,is_rate_predefined,is_rate_subsidized)
VALUES('" . $treatmentTitle . "',". $treatment_department . "," . $treatment_fees . "," .$actual_fees
    .",". $treatment_rate_predefined.",". $treatment_subsidized .")";
$result=mysqli_query($conn,$query) or die(mysqli_error($conn));

$id=mysqli_insert_id($conn);

$response=array();

if($id>0){
    $response["status"]="success";
    $response["message"]=$treatmentTitle . " added ";
}
else{

    $response["status"]="Error";
    $response["message"]="Could not add " . $treatmentTitle . "! Please try again later";
}

echo json_encode($response);
mysqli_close($conn);


?>