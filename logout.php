<?php
/**
 * Created by PhpStorm.
 * User: Rushabh
 * Date: 8/26/2015
 * Time: 11:57 PM
 */

if (isset($_COOKIE['is_user_logged_in'])) {
    unset($_COOKIE['is_user_logged_in']);
    setcookie('is_user_logged_in', null, -1, '/');
}

header('Location: login.php');


?>