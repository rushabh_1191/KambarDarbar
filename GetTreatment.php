<?php

include "db_connect.php";

$department_id= json_decode(file_get_contents("php://input"),true);

$addCondition="";
if(isset($department_id["department_id"])){
    $addCondition=" WHERE t.treatment_department_id=" .$department_id["department_id"];
}
$sql = "SELECT treatment_id,treatment_title,treatment_actual_fees,is_rate_subsidized,is_rate_predefined,department_id,department_name,treatment_fees FROM treatment_information t INNER JOIN department_information d ON t.treatment_department_id =d.department_id ".$addCondition;
$result = $conn->query($sql) or die($conn->error);

    $tId = "treatment_id";
    $ttitle = "treatment_title";
    $data_information=array();
    if ($result->num_rows > 0) {

        $treatmentInformation=array();
        while ($row = $result->fetch_assoc()) {

            $tInfo=array();
            $tInfo["treatment_id"]=$row["treatment_id"];
            $tInfo["treatment_title"]=$row[$ttitle];
            $tInfo["treatment_fees"]=$row["treatment_fees"];
            $tInfo["department_id"]=$row["department_id"];
            $tInfo["is_rate_predefined"]=$row["is_rate_predefined"];
            $tInfo["is_rate_subsidized"]=$row["is_rate_subsidized"];
            $tInfo["department_name"]=$row["department_name"];
            $tInfo["treatment_actual_fees"]=$row["treatment_actual_fees"];
            $data_information[$row["treatment_id"]]=$tInfo;
//            array_push($treatmentInformation,$tInfo);
    }

//        $data_information["data"]=$treatmentInformation;
//        array_push($data_information,$treatmentInformation);


        mysqli_close($conn);
        echo json_encode($data_information);

}
?>
