<?php
/**
 * Created by PhpStorm.
 * User: rushabh
 * Date: 21/07/15
 * Time: 12:36 AM
 */

include "db_connect.php";

$query="SELECT * FROM department_information";

$result=mysqli_query($conn,$query);

$departmentData=array();
$departmentInfo=array();
while($row = $result->fetch_assoc())
{
    $department=array();
    $department["department_id"]=$row["department_id"];
    $department["department_name"]=$row["department_name"];
    array_push($departmentInfo,$department);
}


$departmentData["data"]=$departmentInfo;

echo json_encode($departmentData);
mysqli_close($conn);

?>
